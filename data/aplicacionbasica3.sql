﻿DROP DATABASE IF EXISTS aplicacionbasica3;
CREATE DATABASE IF NOT EXISTS aplicacionbasica3
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

-- 
-- Set default database
--
USE aplicacionbasica3;

--
-- Definition for table fotos
--
CREATE TABLE IF NOT EXISTS fotos (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  portada tinyint(1) DEFAULT 1,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 9
AVG_ROW_LENGTH = 2048
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

--
-- Definition for table paginas
--
CREATE TABLE IF NOT EXISTS paginas (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  texto text DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 4
AVG_ROW_LENGTH = 5461
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

-- 
-- Dumping data for table fotos
--
INSERT INTO fotos VALUES
(1, 'f1.jpg', 1),
(2, 'f2.jpg', 1),
(3, 'f3.jpg', 0),
(4, 'f1.png', 1),
(5, 'f2.png', 1),
(6, 'f3.png', 0),
(7, 'f4.jpg', 0),
(8, 'f5.jpg', 0);

-- 
-- Dumping data for table paginas
--
INSERT INTO paginas VALUES
(1, 'donde estamos', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in tellus vitae eros tristique bibendum ac ac libero. Donec cursus cursus lacus vel accumsan. Vestibulum mi arcu, iaculis in feugiat eget, vehicula quis arcu. Praesent venenatis, metus ut fermentum malesuada, orci felis varius risus, quis dictum tellus arcu a sapien. Nunc suscipit semper orci at lacinia. Vivamus at nisl fringilla, volutpat enim non, feugiat erat. Vivamus elementum tellus arcu, in mollis diam pretium eu. Mauris efficitur justo ac metus volutpat euismod. Aliquam fringilla lacus quis mauris fermentum malesuada. Aenean porta molestie eros, a tristique augue consectetur vel. Sed suscipit dictum imperdiet. Aenean vitae est in mauris maximus sodales ut sed nulla. Aliquam mattis non justo suscipit auctor.\r\n\r\nMorbi vel enim purus. Etiam ut neque luctus, pellentesque nisi sed, scelerisque quam. Vivamus luctus vestibulum urna ut bibendum. Curabitur elit massa, viverra eu pulvinar a, facilisis sit amet arcu. Curabitur sollicitudin libero nec fringilla cursus. Nunc vitae leo nisl. Aenean euismod tortor id dolor sodales, non auctor tellus rhoncus. Suspendisse blandit odio orci, ut interdum lacus malesuada quis. Proin faucibus semper justo at aliquet. Sed gravida augue eget urna convallis, elementum tincidunt lacus ornare. Aliquam at tincidunt nulla, sed efficitur turpis. Donec vel rhoncus nisl.\r\n\r\nProin tincidunt quam in neque fermentum viverra. Pellentesque placerat justo dapibus, commodo nibh eget, maximus ante. Duis ac magna sit amet sem blandit pharetra. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque eget consequat purus. Nullam scelerisque velit lectus, id condimentum dolor molestie id. Mauris ac volutpat libero, eget vehicula nibh. Curabitur ultrices commodo fermentum.\r\n\r\nFusce volutpat ex arcu, vitae mollis mi convallis quis. Aenean tincidunt porta leo eu commodo. Proin molestie sodales luctus. Donec vitae ornare elit. Nulla facilisi. Nullam neque sapien, vulputate in eleifend id, laoreet et augue. In dignissim condimentum ante mollis placerat. Sed ex dui, sagittis id arcu at, feugiat rhoncus augue. Vestibulum libero libero, posuere in ex ut, pellentesque interdum nisl. Nullam nec dapibus felis, sed vestibulum nisi. Vivamus risus nisi, imperdiet in sapien ac, luctus vehicula ex. Sed fringilla porttitor turpis in viverra. Sed quis viverra eros.'),
(2, 'pagina1', '<h2>Esta es la pagina 1</h2>\r\n<div>\r\nEstamos probando\r\n</div>\r\n'),
(3, 'pagina2', 'The standard Lorem Ipsum passage, used since the 1500s\r\n"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."\r\n\r\nSection 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC\r\n"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"');

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;